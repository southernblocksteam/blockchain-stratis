﻿using System;
using System.Net;

using NBitcoin;
using NBitcoin.RPC;

namespace StratisRPCTest
{
    class Test1
    {
        public RPCClient GetRPC()
        {
            // Use the credentials as defined in the node's configuration file (stratis.conf)
            NetworkCredential credentials = new NetworkCredential("user", "password");

            // By default, connect to a node running on the local machine. The RPC port may
            // be differently configured in the configuration file
            RPCClient rpc = new RPCClient(credentials, new Uri("http://[::1]:26174/"), Network.StratisTest);

            return rpc;
        }

        public void Test()
        {
            // Connect to the Stratis node's RPC server
            //RPCClient rpc = GetRPC();

            // Retrieve a transaction from the node's saved blockchain via RPC
            //Transaction sourceTx = rpc.GetRawTransaction(uint256.Parse("d2401ed79279d909fc84b0f89bbec694eff576b9541cc70c717f640238719aef"));

            // Write the string representation of the transaction to the console
            //Console.WriteLine(sourceTx.ToString());
        }

    }

    class Program
    {
        static void Main(string[] args)
        {
            Test1 test1 = new Test1();
            test1.Test();

            Console.ReadLine();
        }
    }
}